const cp = require('child_process');
const path = require('path');
const fs = require('fs');

function printSuccesful() {
  console.log(`💫  Open ${path.resolve(__dirname, 'reactnative/ios/iosapp.xcodeproj')} in Xcode`);
  console.log('And hit the run button');
}

function setup() {
  process.chdir(path.resolve(__dirname));

  var command = cp.spawn(
    path.resolve(__dirname, '../node_modules/.bin/react-native'),
    ['init', 'reactnative']
  );

  process.chdir(path.resolve(__dirname, '..'));

  command.on('exit', function (code, signal) {
    printSuccesful();
  });

  return command;
}

module.exports = function () {
  try {
    var iosApp = fs.statSync(path.resolve(__dirname, 'reactnative'))
  } catch (e) {
    // initialize
    console.log('Initializing iOS');
    iosApp = null;
    setup();
  }

  if (iosApp && iosApp.isDirectory()) {
    printSuccesful();
  }

}
