/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} = React;

var reactnative = React.createClass({
  render: function() {
    return (
      <View>
        <Text>Hi there</Text>
      </View>
    );
  }
});

AppRegistry.registerComponent('reactnative', () => reactnative);
