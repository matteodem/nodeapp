var path = require('path');
var webpack = require('webpack');

module.exports = {
    devtool: 'eval',
    entry: [
      'webpack-dev-server/client?http://localhost:4000',
      'webpack/hot/only-dev-server',
      path.resolve(__dirname, 'app/entry.js')
    ],
    plugins: [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.ProvidePlugin({
        riot: 'riot'
      })
    ],
    output: {
      path: path.resolve(__dirname, 'build'),
      publicPath: 'http://localhost:4000/',
      filename: 'bundle.js'
    },
    module: {
      preLoaders: [
        {
          test: /\.tag$/,
          exclude: /node_modules/,
          loader: 'riotjs-loader',
          query: { type: 'none' }
        }
      ],
      loaders: [
        {
          test: /\.js$|\.tag$/,
          loaders: ['react-hot', 'babel'],
          include: path.join(__dirname, 'app')
        },
        {
          test: /\.css$/,
          loader: 'style!css'
        }
      ]
    }
};
