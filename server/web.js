import fs from 'fs';
import koa from 'koa';
import path from 'path';

const app = koa();

let indexHtml = fs.readFileSync(path.resolve(__dirname, '../app/index.html'), 'utf8');
indexHtml = indexHtml.replace('{baseUrl}', 'http://localhost:4000');

// TODO: add server rendering

// web server
app.use(function *(next) {
  if ('/' == this.request.path) {
    this.body = indexHtml;
    this.status = 200;
  } else {
    yield next;
  }
});

app.use(function *(next) {
  this.body = 'Not found';
  this.status = 404;
});

app.listen(3999);
