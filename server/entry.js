// load babel packages to allow es2015 features (import, export etc.) in the following files
require("babel-polyfill");
require('babel-register');

require('./web.js');
