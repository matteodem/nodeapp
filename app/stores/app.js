import { createStore, combineReducers } from 'redux';
import { reducer as todos } from './todos.js';

export default createStore(
  combineReducers({
    todos
  })
);
