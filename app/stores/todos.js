import { createStore } from 'redux';
import { remove } from 'lodash';

export function reducer (state = [], action) {
  switch (action.type) {
    case 'ADD_TASK':
      if (action.text.length == 0) {
        return state;
      }

      return [
        ...state,
        {
          text: action.text
        }
      ];
    case 'REMOVE_TASK':
      return remove(state, (val, index) => index != action.index);
    default:
      return state;
  }
};

export default createStore(reducer);
