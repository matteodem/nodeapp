import './todo.css';
import { map } from 'lodash';
import React, { Component } from 'react';
import store from '../../../stores/app.js';

export default class Todo extends Component {
  addTodo(e) {
    e.preventDefault();
    store.dispatch({ type: 'ADD_TASK', text: this.input.value });
    this.input.value = '';
  }

  removeTodo(e, index) {
    e.preventDefault();
    store.dispatch({ type: 'REMOVE_TASK', index });
  }

  render() {
    return (
      <div>
        <ul>
          {this.props.todos.map((todo, index) =>
            <li key={index}>
              {todo.text}
              <button onClick={ event => { this.removeTodo(event, index) } }>Remove</button>
            </li>
          )}
        </ul>
        <form onSubmit={ event => { this.addTodo(event) } }>
          <input ref={ node => { this.input = node; } } />
          <button>Add todo</button>
        </form>
      </div>
    );
  }
}
