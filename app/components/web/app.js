import Todo from './todo/todo.js';
import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        <h1>Todo App</h1>
        <Todo {...this.props.state} />
      </div>
    );
  }
}
