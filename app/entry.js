import App from './components/web/app.js';

import React from 'react';
import ReactDOM from 'react-dom';
import store from './stores/app.js';

// TODO: react redbox?
// TODO: fix testing environment
// TODO: use app store with combineReducers that takes all reducers in account!
// TODO: use https://github.com/primus/primus
const render = () => {
  const root = document.getElementById('app');
  ReactDOM.render(<App state={store.getState()} />, root);
}

store.subscribe(render);
render();
