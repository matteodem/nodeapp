const cp = require('child_process');
const path = require('path');

module.exports = function () {
  var electron = cp.spawn(path.resolve(__dirname, '../node_modules/.bin/electron'), [
    path.resolve(__dirname, '..')
  ]);

  electron.on('close', function (code, signal) {
    console.log('Electron terminated due to ' + signal);
  });

  console.log('💫  Opening a frame');

  return electron;
}
