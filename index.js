const nodemon = require('nodemon');
const initBundler = require ('./bundler.js');

var target = process.argv[2] || 'web';

if ('desktop' == target) {
  require('./desktop/entry.js')();
} else if ('ios' == target) {
  require('./phone/ios.entry.js')();
} else {
  // target the web
  console.log('💫  Go to http://localhost:4000');
}

initBundler(target);

// listen to server side changes
nodemon({
  script: 'server/entry.js',
  ext: 'js json',
  ignore: ['app/*', 'phone/*', 'desktop/*']
});

nodemon.on('start', function () {
  //console.log('App started');
}).on('quit', function () {
  console.log('App quit');
}).on('restart', function (files) {
  console.log('App restarting: ');

  files.forEach(function (file) {
    console.log(`\t ${file} changed`);
  });
});
