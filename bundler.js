const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const webpackTargetElectronRenderer = require('webpack-target-electron-renderer');
const webpackConfig = require('./webpack.config.js');

module.exports = function (target) {
  if ("desktop" == target) {
    webpackConfig = webpackTargetElectronRenderer(webpackConfig);
  }

  // Module bundler
  new WebpackDevServer(webpack(webpackConfig), {
     hot: true,
     historyApiFallback: true,
     quiet: true,
     proxy: {
       "*": "http://localhost:3999"
     }
  }).listen(4000, 'localhost', function (err, result) {
     if (err) {
       console.log(err);
     }
  });
};
